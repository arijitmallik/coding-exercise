package com.gamesys;

import com.gamesys.models.RegistrationAPIConfig;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class AppConfiguration extends Configuration {
    @Valid
    @NotNull
    private RegistrationAPIConfig apiConfig;

    @JsonProperty("registrationApiConfig")
    public RegistrationAPIConfig getApiConfig() {
        return this.apiConfig;
    }

    @JsonProperty("registrationApiConfig")
    public void setApiConfig(RegistrationAPIConfig apiConfig) {
        this.apiConfig = apiConfig;
    }
}
