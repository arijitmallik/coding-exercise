package com.gamesys.services;

import com.google.inject.Inject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExclusionService {
    @Inject
    ExclusionService(){}

    private static SimpleDateFormat dateOfBirthFormat;

    public static void setDateOfBirthFormat(SimpleDateFormat dateOfBirthFormat) {
        ExclusionService.dateOfBirthFormat = dateOfBirthFormat;
        if(dateOfBirthFormat == null)
           dateOfBirthFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateOfBirthFormat.setLenient(false);
    }

    /**
     * Validates where a user is blacklisted.
     *
     * For simplicity, let's have a simple rule.
     * If a user's DOB is before 1980, return false.
     * If a user's ssn length is lesser than 9, return false.
     *
     * TODO RegistrationAPIConfig can have these as configs.
     *
     * @param dob - Date of Birth.
     * @param ssn - SSN.
     * @return whether the user is blacklisted or not. Returns false if blacklisted, true otherwise.
     */
    public boolean validate(String dob, String ssn){
        Date dateOfBirth;
        try{
            dateOfBirth = dateOfBirthFormat.parse(dob);
        } catch(Exception ex){
            return false;
        }

        if(dateOfBirth.before(new GregorianCalendar(1980, Calendar.JANUARY, 1).getTime())){
            return false;
        }

        if(ssn.length() < 9){
            return false;
        }

        return true;
    }
}
