package com.gamesys.services;

import com.gamesys.models.User;
import com.gamesys.storage.UserStore;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserService {
    @Inject
    UserService(){}

    private static UserStore userStore = new UserStore();
    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    public static void initializeStore(){
        // Initialize the user store, which is in memory for now.
        try{
            userStore.initializeStore();
        } catch(UnsupportedOperationException ex){
            logger.warn("User store already initialized");
        } catch(Exception ex){
            logger.error("User store initialization unexpected error: {}",ex.getLocalizedMessage(),ex.fillInStackTrace());
            throw ex;
        }
    }

    public boolean doesUserExist(String userName){
        return userStore.doesUserExistInStore(userName);
    }

    public void addUser(String userName, User user){
        userStore.addUserInStore(user.getUserName(), user);
    }
}
