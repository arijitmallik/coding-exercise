package com.gamesys.storage;

import com.gamesys.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserStore {
    private Map<String, User> userStore = null;

    public void initializeStore(){
        if(userStore != null){
            throw new UnsupportedOperationException("User Store Not Empty!");
        }
        userStore = new HashMap<>();
    }

    public boolean doesUserExistInStore(String username){
        return userStore.containsKey(username);
    }

    public void addUserInStore(String username,User user){
        userStore.put(username, user);
    }
}
