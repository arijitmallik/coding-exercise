package com.gamesys;

import com.gamesys.guice.ConfigurationModule;
import com.gamesys.resources.RegistrationResource;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class App extends Application<AppConfiguration> {
    public static void main(final String[] args) throws Exception {
        new App().run(args);
    }

    private Injector guice;

    @Override
    public String getName() {
        return "registration_api";
    }

    @Override
    public void initialize(final Bootstrap<AppConfiguration> bootstrap) {
        // TODO: We can add bundles here, e.g. Health Check, metadata catalog, etc.
    }

    @Override
    public void run(final AppConfiguration configuration, final Environment environment) {
        // Resource Registration
        guice = createGuiceInjector(configuration);
        JerseyEnvironment jersey = environment.jersey();
        jersey.register(guice.getInstance(RegistrationResource.class));

    }

    private Injector createGuiceInjector(final AppConfiguration configuration) {
        ConfigurationModule configurationModule = new ConfigurationModule(configuration);
        return Guice.createInjector(configurationModule);
    }

}
