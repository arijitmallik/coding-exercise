package com.gamesys.resources;

import com.gamesys.models.RegistrationAPIConfig;
import com.gamesys.models.User;
import com.gamesys.services.ExclusionService;
import com.gamesys.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/register")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RegistrationResource {
    private final static Logger logger = LoggerFactory.getLogger(RegistrationResource.class);
    private UserService userService;
    private ExclusionService exclusionService;
    private RegistrationAPIConfig apiConfig;

    @Inject
    public RegistrationResource(UserService userService, ExclusionService exclusionService, RegistrationAPIConfig apiConfig){
        this.userService = userService;
        this.exclusionService = exclusionService;
        this.apiConfig = apiConfig;
        userService.initializeStore();
        ExclusionService.setDateOfBirthFormat(apiConfig.getDateFormat());
        User.setDateOfBirthFormat(apiConfig.getDateFormat());
    }

    /**
     * The POST method which registers the user.
     *
     * @param userName - Username of the user. Alphanumerical, no spaces.
     * @param password - Password of the user. At least four characters, at least one upper case character, at least one number.
     * @param dateOfBirth - DOB of the user. Format expected - "yyyy/mm/dd". Should be a valid date.
     * @param SSN - SSN of the user.
     * @return
     *
     * TODO Can change the method to accept the User as a json. This is accepting the different fields of the User as the query parameters.
     */
    @POST
    public Response register(@QueryParam("username") String userName, @QueryParam("password") String password, @QueryParam("dateOfBirth") String dateOfBirth, @QueryParam("SSN") String SSN){
        User user;
        try{
            user = new User.UserBuilder().withUserName(userName).withPassword(password)
                    .withDateOfBirth(dateOfBirth).withSSN(SSN).build();
        } catch(IllegalArgumentException ex){
            logger.warn("Exception while entering user details: {}",ex.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        } catch(Exception ex){
            logger.error("Unexpected exception: {}",ex.getMessage(),ex.fillInStackTrace());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }

        // Check if the user already exists
        if(userService.doesUserExist(user.getUserName())){
            return Response.status(Response.Status.CONFLICT).entity("User already exists!").build();
        }

        // Check if the user is blacklisted
        if(!exclusionService.validate(user.getDateOfBirth(), user.getSSN())){
            return Response.status(Response.Status.FORBIDDEN).entity("User is BlackListed!").build();
        }

        // Things look good. Add the user.
        userService.addUser(user.getUserName(), user);
        return Response.status(Response.Status.ACCEPTED).build();
    }
}
