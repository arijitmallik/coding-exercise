package com.gamesys.guice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesys.AppConfiguration;
import com.gamesys.models.RegistrationAPIConfig;
import io.dropwizard.setup.Environment;
import com.google.inject.Binder;
import com.google.inject.Module;

/**
 * Module for injecting configuration and environmental data
 */
public class ConfigurationModule implements Module {
    private final AppConfiguration configuration;
    public ConfigurationModule(final AppConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void configure(Binder binder) {
        binder.bind(RegistrationAPIConfig.class).toInstance(configuration.getApiConfig());
    }
}
