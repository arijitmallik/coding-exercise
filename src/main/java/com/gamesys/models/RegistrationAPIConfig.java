package com.gamesys.models;

import java.text.SimpleDateFormat;

public class RegistrationAPIConfig {
    private SimpleDateFormat dateFormat;

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }
}
