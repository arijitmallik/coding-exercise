package com.gamesys.models;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class User {
    private String userName;
    private String password;
    private String dateOfBirth;
    private String SSN;
    private int id = 1;

    private static SimpleDateFormat dateOfBirthFormat;

    public static void setDateOfBirthFormat(SimpleDateFormat dateOfBirthFormat) {
        User.dateOfBirthFormat = dateOfBirthFormat;
        if(dateOfBirthFormat == null)
            dateOfBirthFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateOfBirthFormat.setLenient(false);
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getSSN() {
        return SSN;
    }

    //TODO: Support user details modification option by adding setters.

    public User(UserBuilder builder) {
        this.userName = builder.userName;
        this.password = builder.password;
        this.dateOfBirth = builder.dateOfBirth;
        this.SSN = builder.SSN;
    }

    private static final String userErrorMessageFormat = "%s is wrong. Correct way: %s";

    /*
     * This method validates that all fields of a User are correctly set.
     * TODO: We can use this validate even when user's details are modified.
     */
    private void validate() throws IllegalArgumentException {
        if(!isValidUserName(this.userName)) {
            String errorMessage = String.format(userErrorMessageFormat, "Username", "Must be alphanumeric without any spaces.");
            throw new IllegalArgumentException(errorMessage);
        }

        if(!isValidPassword(this.password)) {
            String errorMessage = String.format(userErrorMessageFormat, "Password", "Must have at least four characters, at least one upper case character, at least one number.");
            throw new IllegalArgumentException(errorMessage);
        }

        if(!isValidDateOfBirth(this.dateOfBirth)) {
            String errorMessage = String.format(userErrorMessageFormat, "DateOfBirth", "Must have the format "+dateOfBirthFormat.toPattern()); // TODO Use efficient method than + to concatenate strings if needed.
            throw new IllegalArgumentException(errorMessage);
        }

        if(!isValidSSN(this.SSN)) {
            String errorMessage = String.format(userErrorMessageFormat, "SSN", "Should be non-empty");
            throw new IllegalArgumentException(errorMessage);
        }

    }

    // Customized validators for each of the fields.

    // userName - Alphanumerical, no spaces.
    private boolean isValidUserName(String userName){
        if(StringUtils.isEmpty(userName))
            return false;
        if(StringUtils.containsWhitespace(userName))
            return false;
        if(!StringUtils.isAlphanumeric(userName))
            return false;
        return true;
    }

    // password - At least four characters, at least one upper case character, at least one number.
    private boolean isValidPassword(String password){
        if(StringUtils.isEmpty(password))
            return false;
        if(password.length()<4)
            return false;
        if(!Pattern.compile("[0-9]").matcher(password).find())
            return false;
        if(!Pattern.compile("[A-Z]").matcher(password).find())
            return false;
        return true;
    }

    // dateOfBirth - Format expected. Should be a valid date.
    private boolean isValidDateOfBirth(String dateOfBirth){
        if(StringUtils.isEmpty(dateOfBirth))
            return false;
        try{
            Date date = dateOfBirthFormat.parse(dateOfBirth);
        } catch(ParseException ex){
            return false;
        }
        return true;
    }

    // SSN - Should have non-empty value. No other validation for now.
    private boolean isValidSSN(String SSN) {
        if(SSN == null)
            return false;
        return true;
    }

    /*
     * I was initially thinking that the Map/local store should store the user as key and a boolean as value.
     * So, I implemented that and needed to use equals and hashcode.
     * However, later realized that for sanity, username should be the key, and the value should be User. :)
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(!(obj instanceof User))
            return false;
        User other = (User) obj;

        return this.userName.equals(other.userName)
                && this.password.equals(other.password)
                && this.dateOfBirth.equals(other.dateOfBirth)
                && this.SSN.equals(other.SSN);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        return builder.append(this.getClass().getSimpleName())
                .append("[Username:")
                .append(this.userName)
                .append(",Password:")
                .append(this.password)
                .append(",DateOfBirth:")
                .append(this.dateOfBirth)
                .append(",SSN:")
                .append(this.SSN)
                .append("]").toString();
    }

    /*
     * Builder class for user.
     * Using builder design pattern since users might eventually have a lot of attributes.
     */
    public static class UserBuilder {
        private String userName;
        private String password;
        private String dateOfBirth;
        private String SSN;

        public UserBuilder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder withDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public UserBuilder withSSN(String SSN) {
            this.SSN = SSN;
            return this;
        }

        public User build() throws IllegalArgumentException{
            User user = new User(this);
            user.validate();
            return user;
        }
    }
}