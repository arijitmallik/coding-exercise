package com.gamesys.resources;

import com.gamesys.models.RegistrationAPIConfig;
import com.gamesys.models.User;
import com.gamesys.services.ExclusionService;
import com.gamesys.services.UserService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RegistrationResourceTest {

    private static final UserService userService  = mock(UserService.class);
    private static final ExclusionService exclusionService  = mock(ExclusionService.class);
    private static final RegistrationAPIConfig registrationAPIConfig  = mock(RegistrationAPIConfig.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new RegistrationResource(userService, exclusionService, registrationAPIConfig))
            .build();

    @Before
    public void setup() {
        User.setDateOfBirthFormat(new SimpleDateFormat("yyyy-MM-dd"));
        when(userService.doesUserExist(eq("Arijit"))).thenReturn(false);
        when(exclusionService.validate(eq("1991-04-22"),eq("123456789"))).thenReturn(true);
    }

    @After
    public void tearDown(){
        reset(userService);
        reset(exclusionService);
        reset(registrationAPIConfig);
    }

    @Test
    public void testPostValidUser() {
        assertEquals(202, resources.target("/register")
                .queryParam("username","Arijit")
                .queryParam("password","Arijit12")
                .queryParam("dateOfBirth","1991-04-22")
                .queryParam("SSN","123456789").request().post(null).getStatus());
    }

    @Test
    public void testPostForbiddenUser() {
        when(exclusionService.validate(eq("1991-04-22"),eq("12345679"))).thenReturn(false);
        assertEquals(403, resources.target("/register")
                .queryParam("username","Arijit")
                .queryParam("password","Arijit12")
                .queryParam("dateOfBirth","1991-04-22")
                .queryParam("SSN","12345679").request().post(null).getStatus());
    }

    @Test
    public void testPostDuplicateUser() {
        when(userService.doesUserExist(eq("Arijit"))).thenReturn(true);
        assertEquals(409, resources.target("/register")
                .queryParam("username","Arijit")
                .queryParam("password","Arijit12")
                .queryParam("dateOfBirth","1991-04-22")
                .queryParam("SSN","12345679").request().post(null).getStatus());
    }
}