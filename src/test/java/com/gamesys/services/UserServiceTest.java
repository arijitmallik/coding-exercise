package com.gamesys.services;

import com.gamesys.models.User;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

public class UserServiceTest {
    private UserService userService;

    @Before
    public void setup(){
        User.setDateOfBirthFormat(new SimpleDateFormat("yyyy-MM-dd"));
        UserService.initializeStore();
        userService = new UserService();
    }

    @Test
    public void testUserAdditionAndExistence(){
        User user1 = new User.UserBuilder().withUserName("Arijit1").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        User user2 = new User.UserBuilder().withUserName("Arijit1").withPassword("Arijit1233")
                .withDateOfBirth("1991-04-21").withSSN("123456789").build();
        User user3 = new User.UserBuilder().withUserName("Arijit2").withPassword("Arijit3")
                .withDateOfBirth("1991-04-22").withSSN("123456789").build();

        userService.addUser(user1.getUserName(), user1);
        assertEquals(true, userService.doesUserExist(user1.getUserName()));
        assertEquals(true, userService.doesUserExist(user2.getUserName()));
        assertEquals(false, userService.doesUserExist(user3.getUserName()));
    }
}
