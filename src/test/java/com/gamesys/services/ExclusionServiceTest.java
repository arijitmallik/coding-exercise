package com.gamesys.services;

import com.gamesys.models.User;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;

public class ExclusionServiceTest {
    private ExclusionService exclusionService;

    @Before
    public void setup(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        User.setDateOfBirthFormat(format);
        ExclusionService.setDateOfBirthFormat(format);
        exclusionService = new ExclusionService();
    }

    @Test
    public void testWhiteListedUser(){
        User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        assertEquals(true, exclusionService.validate(user.getDateOfBirth(), user.getSSN()));
    }

    @Test
    public void testBlackListedUser(){
        User user1 = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("1234589").build();
        assertEquals(false, exclusionService.validate(user1.getDateOfBirth(), user1.getSSN()));

        User user2 = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1971-04-11").withSSN("123456789").build();
        assertEquals(false, exclusionService.validate(user2.getDateOfBirth(), user2.getSSN()));
    }
}