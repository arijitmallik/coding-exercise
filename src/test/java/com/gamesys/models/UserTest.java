package com.gamesys.models;

import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class UserTest {
    @Before
    public void setup(){
        User.setDateOfBirthFormat(new SimpleDateFormat("yyyy-MM-dd"));
    }

    @Test
    public void createValidUser(){
        User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        assertEquals("Arijit", user.getUserName());
        assertEquals("Arijit12", user.getPassword());
        assertEquals("1991-04-11", user.getDateOfBirth());
        assertEquals("123456789", user.getSSN());
    }

    @Test
    public void testUserEquals(){
        User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        assertEquals(user, new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                .withDateOfBirth("1991-04-11").withSSN("123456789").build());
    }

    @Test
    public void testUsernameValidations(){
        // null
        try{
            User user = new User.UserBuilder().withUserName(null).withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Username is wrong. Correct way: Must be alphanumeric without any spaces.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // empty
        try{
            User user = new User.UserBuilder().withUserName(null).withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Username is wrong. Correct way: Must be alphanumeric without any spaces.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // Special Character
        try{
            User user = new User.UserBuilder().withUserName("A@rijit").withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Username is wrong. Correct way: Must be alphanumeric without any spaces.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // Spaces
        try{
            User user = new User.UserBuilder().withUserName("Arijit Mallik").withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Username is wrong. Correct way: Must be alphanumeric without any spaces.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }
    }

    @Test
    public void testPasswordValidations(){
        // null
        try{
            User user = new User.UserBuilder().withUserName("Arijit").withPassword(null)
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Password is wrong. Correct way: Must have at least four characters, at least one upper case character, at least one number.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // empty
        try{
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Password is wrong. Correct way: Must have at least four characters, at least one upper case character, at least one number.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // Length < 4
        try{
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("ari")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Password is wrong. Correct way: Must have at least four characters, at least one upper case character, at least one number.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // No Uppercase
        try{
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Password is wrong. Correct way: Must have at least four characters, at least one upper case character, at least one number.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }

        // No Numbers
        try{
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit")
                    .withDateOfBirth("1991-04-11").withSSN("123456789").build();
        } catch(IllegalArgumentException ex){
            assertEquals("Password is wrong. Correct way: Must have at least four characters, at least one upper case character, at least one number.", ex.getMessage());
        } catch(Exception ex){
            fail("Unexpected!");
        }
    }

    @Test
    public void testDateOfBirthValidations() {
        // null
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth(null).withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // Empty
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("").withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // Random format
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("19910411").withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // Invalid day
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("1991-04-111").withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // Invalid month
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("1991-44-11").withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // Invalid year
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("4999-4-11").withSSN("123456789").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("DateOfBirth is wrong. Correct way: Must have the format yyyy-MM-dd", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }
    }

    @Test
    public void testSSNValidations() {
        // null
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN(null).build();
        } catch (IllegalArgumentException ex) {
            assertEquals("SSN is wrong. Correct way: Should be non-empty", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }

        // empty
        try {
            User user = new User.UserBuilder().withUserName("Arijit").withPassword("Arijit12")
                    .withDateOfBirth("1991-04-11").withSSN("").build();
        } catch (IllegalArgumentException ex) {
            assertEquals("SSN is wrong. Correct way: Should be non-empty", ex.getMessage());
        } catch (Exception ex) {
            fail("Unexpected!");
        }
    }
}
