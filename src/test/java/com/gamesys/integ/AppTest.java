package com.gamesys.integ;

import com.gamesys.App;
import com.gamesys.AppConfiguration;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class AppTest {
    @ClassRule
    public static final DropwizardAppRule<AppConfiguration> RULE =
            new DropwizardAppRule<>(App.class, ResourceHelpers.resourceFilePath("test_config.yml"));

    @Test
    public void loginHandlerRedirectsAfterPost() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Registration API - Test");

        Response response = client.target(
                String.format("http://localhost:%d/register", RULE.getLocalPort()))
                .queryParam("username","Arijit")
                .queryParam("password","Arijit12")
                .queryParam("dateOfBirth","1991-04-22")
                .queryParam("SSN","123456789")
                .request()
                .post(null);

        assertEquals(202, response.getStatus());
    }
}
