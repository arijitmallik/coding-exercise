Building the project:
mvn clean package

Running the jar:
java -jar target/registration_api-0.0.1-SNAPSHOT.jar server registration_api_config.yml

Make sure the registration_api_config.yml is present in your project home dir. The jar is generated in the target. It is already included and ready to run.

The webapp runs in port 19999. You can configure it in the config file.

Assumptions:
Users are considered blacklisted if the DOB is before 1980 Jan 01 or SSN is lesser than 9 digits.

Validations:
All validations have been implemented, as specified.

Making requests and Sample Outputs:
curl -X POST 'http://127.0.0.1:19999/register?username=Arijit&password=Arijit23&dateOfBirth=1991-04-22&SSN=123456789
Output: empty, Status:202.

curl -X POST 'http://127.0.0.1:19999/register?username=Arijit&password=Arijit123&dateOfBirth=1991-04-11&SSN=123456789'
Output: User already exists!, Status: 409.

curl -X POST 'http://127.0.0.1:19999/register?username=Arijit11&password=Arijit123&dateOfBirth=1991-04-11&SSN=1256789'
Output: User is BlackListed!, Status: 403.
